# README #

code is checked out using Ansible and deployed to a virtual machine

1. Apache2, PHP5, Mysql and related mods are installed
2. This code is cloned to var/www/ansibletest
3. new site-available is created
4. symlink is created to enable (site-enable)
5. Apache2 is restarted

**Note**: Create entry in local hosts file to map ansible web server to ansibletest.com